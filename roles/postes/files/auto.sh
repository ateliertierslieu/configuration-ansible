#!/bin/bash
TITLE="Session d'invité temporaire"
TEXT="Bienvenue au Pôle numérique de l'Atelier Tiers-Lieu\n\n
Toutes les données créées durant cette session d'invité seront
supprimées lorsque vous vous déconnecterez, et les paramètres
seront réinitialisés.
Si vous souhaitez retrouver vos fichiers plus tard, veuillez
les sauvegarder sur un support externe, par exemple sur
une clé USB."
zenity --warning --no-wrap --title="$TITLE" --text="$TEXT"
