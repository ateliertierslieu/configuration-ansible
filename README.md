# Configuration Ansible

## Présentation

Ce dépôt contient les configurations Ansible applicables aux différents équipements internes à l'Atelier.

Il vise notamment à :

* Configurer les postes en libre accès fonctionnant avec Linux Mint
* Configurer la machine gérant les services partagés


## Les postes en libre accès

Voici les principaux points de configuration appliqués aux postes en libre accès :

* Configuration de la mise à jour automatique par l'utilitaire `Update Manager` intégré à Linux Mint.
* Création d'un utilisateur invité, dont le profil est vidé lors de chaque déconnexion et démarrage du poste.
* Autorise les utilisateurs invité et `utilisateur` à créer des partages de dossiers en réseau avec les utilisateurs des autres postes.
